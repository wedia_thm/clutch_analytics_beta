package clutch;

import com.noheto.extensions.interfaces.services.AbstractWorkflowTriggerBusinessService;

import com.wedia.wxm.bean.ProjectBean;
import com.wedia.wxm.database.IObjectStructureInfo;
import com.wedia.wxm.database.commons.DataModel;
import noheto.workflow.IState;
import noheto.workflow.IWorkflow;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import wsnoheto.config.CTConfig;
import wsnoheto.engine.*;
import wsnoheto.log.LoggingService;
import wsnoheto.log.analytics.EventsLogger;

import java.util.HashSet;
import java.util.Set;

public class CollateralAnalyticsWorkflowBusinessService extends AbstractWorkflowTriggerBusinessService {

    private final static Logger logger = LoggingService.getLogger("wms");

    private static Set<String> availableWorkflowNames = null;


    private static Set<String> getAvailableWorkflowNames() {
        if (availableWorkflowNames == null){
            Set<String> awn = new HashSet<String>();
            try {
                if (CTObjectStructure.isExists(DataModel.Object.PROJECT.getType())) {
                    CTObjectStructure structure = CTObjectStructure.load(DataModel.Object.PROJECT.getType());
                    awn.add(structure.getField(7).getNature().toString());
                }
                if (CTObjectStructure.isExists(DataModel.Object.CHANNEL.getType())) {
                    CTObjectStructure channel = CTObjectStructure.load(DataModel.Object.CHANNEL.getType());
                    if (channel.isExistField( "nature" ) && channel.isExistField("modelnature")){
                        Set<String> natures = new HashSet<String>();
                        for(CTObject o : BatchObjectsIterable.all(DataModel.Object.CHANNEL.getType()).where( PreparedWhere.load("? AND ?").addStringNotEmpty( "nature" ).addStringNotEmpty("modelnature") ).select("pnature") ){
                            natures.add(o.getProperty("nature"));
                        }
                        natures.forEach( nature -> {
                            try {
                                awn.add(CTObjectStructure.load(nature).getField(7).getNature().toString());
                            }catch (Throwable eex){
                            }
                        });
                    }
                }
            }catch (Throwable e){
            }
            availableWorkflowNames = new HashSet<String>();
            availableWorkflowNames.addAll(awn);
        }
        return availableWorkflowNames;
    }

    @Override
    public boolean executeOnWorkflowName(String name){
        return !CTConfig.C_PARAM_DEV_LOG_ACTION_ON_WORKFLOW_PROCESS && getAvailableWorkflowNames().contains( name );
    }

    private void handleMedia(CTSurfer surfer, IObjectWritable media){

        // on s'assure que l'objet channel existe
        if (!CTObjectStructure.isExists(DataModel.Object.CHANNEL.getType())) return;
        // on s'assure que notre media possède bien une propriete channel
        if (!media.isPropertyAvailable( DataModel.Property.CHANNEL.getColField() )) return;
        // on s'assure que la nature de la propriété channel est bien l'objet channel
        if (!media.getField(DataModel.Property.CHANNEL.getColField()).getNature().equalsIgnoreCase(DataModel.Object.CHANNEL.getType())) return;

        try{
            CTObject channel = (CTObject)media.getPropertyAsObject( DataModel.Property.CHANNEL.getColField() );
            if (!channel.isPropertyAvailable( "modelnature" )) return;
            if (!channel.isPropertyAvailable( "nature" )) return;
            String nature = channel.getProperty("nature");
            if (StringUtils.isBlank(nature)) return;
            String modelNature = channel.getProperty("modelnature");
            if (StringUtils.isBlank(modelNature)) return;
            ProjectBean project = new ProjectBean(media);
            // si les collaterals ont un model (propriété model pointant vers un objet référencé en tant que modelnature
            // dans un channel alors on va pouvoir logger un used_media_published

            project.getCollateralsInfos().stream().filter(collateral ->{
                IObjectStructureInfo cos = collateral.getObjectStructureInfo();
                return ( cos != null &&  cos.getObjectType().equalsIgnoreCase(nature) && cos.isManagedCopy() && cos.isExistField(DataModel.Property.MODEL));
            }).forEach( collateral -> {
                try {
                    handleCollateral(surfer, collateral.getCTObject());
                }catch (Throwable xx){
                    //
                }
            });
        }catch (Throwable e){
            if (logger != null) logger.error(e.getMessage() , e);
        }
    }
    private void handleCollateral(CTSurfer surfer, IObjectReadOnly collateral ){
        try {
            if (!collateral.isPropertyAvailable("model")) return;

            CTObject model = (CTObject) collateral.getPropertyAsObject("model");
            if (model == null) return;
            
            EventsLogger.logSysObjectDataEvent( surfer , "used_media_published" , model );

        }catch (Throwable ex){
            if (logger != null) logger.error(ex.getMessage() , ex);
        }
    }

    @Override
    public void onWorkflow_afterEnterTrigger(CTSurfer surfer, IWorkflow workflow, IState current_state) {
        if (!current_state.isOnline()) return;
        IObjectWritable activeObject = workflow.getOwner();
        if (activeObject == null) return;
        if (DataModel.Object.PROJECT.getType().equalsIgnoreCase(activeObject.getObjectType().toString())){
            this.handleMedia(surfer, activeObject);
        }else{
            if ( getAvailableWorkflowNames().contains( activeObject.getField(7).getNature().toString() ) ){
                this.handleCollateral(surfer , activeObject);
            }
        }

    }
}
