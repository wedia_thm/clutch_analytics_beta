package clutch;

import com.noheto.extensions.interfaces.services.AbstractObjectTriggerBusinessService;
import com.wedia.wxm.bean.ObjectStructureInfo;
import org.apache.commons.lang3.StringUtils;

import wsnoheto.config.CTConfig;
import wsnoheto.engine.*;
import wsnoheto.log.analytics.EventsLogger;

public class CollateralAnalyticsObjectTrigger extends AbstractObjectTriggerBusinessService {

    private static CTObjectStructure channel = null;

    @Override
    public boolean executeOnInsert(IObjectStructureReadOnly structure, IObjectTableReadOnly table) throws Throwable {
        if(CTConfig.C_PARAM_DEV_LOG_ACTION_ON_WORKFLOW_PROCESS) return false;
        
        if (channel == null && CTObjectStructure.isExists("channel")){
            channel = CTObjectStructure.load("channel");
        }
        if( channel != null
                && channel.isExistField( "nature" )
                && channel.isExistField( "modelnature" )
                && !table.isRemote()
                && structure.isExistField( "model" )
                && ObjectStructureInfo.getObjectStructureInfo(structure).isManagedCopy()
        ){
            IObjectField model = structure.getField("model" );
            String nature = model.getNature().toString();
            return (!StringUtils.isBlank(nature));
        }
        return false;
    }

    @Override
    public void onInsertEnd(IObjectWritable oActiveObject, CTSurfer oSurfer) throws Throwable {

        PreparedWhere where = PreparedWhere.load( "? AND ?" )
                .addStringEquals( "modelnature" , oActiveObject.getField("model").getNature().toString() )
                .addStringEquals("nature", oActiveObject.getObjectType().toString());
        if (CTObjects.count( "channel" , where , 1 ) == 0){
            return;
        }
        CTObject model = (CTObject)oActiveObject.getPropertyAsObject("model" );
        if (model == null) return;
        try {
            EventsLogger.logSysObjectDataEvent(oSurfer, "used_media_created", model);
        }catch (Throwable e){
            //
        }
    }
}
