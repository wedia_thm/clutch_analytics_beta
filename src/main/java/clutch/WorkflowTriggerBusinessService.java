package clutch;

import com.noheto.extensions.interfaces.services.AbstractWorkflowTriggerBusinessService;
import noheto.workflow.IAction;
import noheto.workflow.IState;
import noheto.workflow.IWorkflow;
import wsnoheto.config.CTConfig;
import wsnoheto.engine.CTSurfer;
import wsnoheto.log.analytics.EventsLogger;

public class WorkflowTriggerBusinessService extends AbstractWorkflowTriggerBusinessService {
	private IAction action;
	
	@Override
	public boolean executeOnWorkflowName(String name) {
		// si la version du moteur est vieille
		return !CTConfig.C_PARAM_DEV_LOG_ACTION_ON_WORKFLOW_PROCESS;
	}

	@Override
	public boolean onWorkflow_afterTransitionTrigger(CTSurfer user, IWorkflow workflow, IAction action) {
		if (!CTConfig.C_PARAM_DEV_LOG_ACTION_ON_WORKFLOW_PROCESS) {
			this.action=action;
		}
		return super.onWorkflow_afterTransitionTrigger(user, workflow, action);
	}

	@Override
	public void onWorkflow_afterEnterTrigger(CTSurfer user, IWorkflow workflow, IState current_state) {
		// si la version du moteur est vieille
		if (!CTConfig.C_PARAM_DEV_LOG_ACTION_ON_WORKFLOW_PROCESS && this.action!=null) {
			EventsLogger.logSysObjectDataEvent(user, "wkf_"+this.action.getName(), workflow.getOwner());
		}	
	}
	
}