Ce plugin ne doit �tre activ� que sur un war 11.16.x

Ajouter les clefs suivantes dans le fichier wcm.properties de la SAN (en perso) et activer le plugin

analytics.dashboards.button.clutch_w2p_template=Web To Print (templates)
analytics.dashboards.button.clutch_w2p_document=Web To Print (documents)
analytics.dashboard.sys_w2p_template.title=All actions
analytics.dashboard.sys_w2p_template.chart.histogram.title=All web-to-print actions
analytics.dashboard.sys_w2p_template.chart.histogram.aggregationlabel=All actions
analytics.dashboard.sys_w2p_document.title=All actions
analytics.dashboard.sys_w2p_document.chart.histogram.title=All web-to-print actions
analytics.dashboard.sys_w2p_document.chart.histogram.aggregationlabel=All actions
analytics.dashboards.button.sys_w2p_template=Web To Print (templates)
analytics.dashboards.button.sys_w2p_document=Web To Print (documents)
basictermsvaluesformatter.sys_objectdata.actionid.used_media_created=Project created
basictermsvaluesformatter.sys_objectdata.actionid.used_media_published=Project published
basictermsvaluesformatter.sys_objectdata.actionid.w2p_cico_checkin=Checkin
basictermsvaluesformatter.sys_objectdata.actionid.w2p_cico_checkout=Checkout
basictermsvaluesformatter.sys_objectdata.actionid.w2p_cico_cancel=Checkin canceled
basictermsvaluesformatter.sys_objectdata.actionid.w2p_snippet_import=Snippet used
basictermsvaluesformatter.sys_objectdata.actionid.w2p_download_pdf=PDF Download
basictermsvaluesformatter.sys_objectdata.actionid.w2p_dylation_execute=Dylation (execution)
basictermsvaluesformatter.sys_objectdata.actionid.w2p_dylation_undo=Dylation (cancel)
basictermsvaluesformatter.sys_objectdata.actionid.w2p_dylation_resize=Dylation (resize)
basictermsvaluesformatter.sys_objectdata.actionid.w2p_user_sendwxml=Composition



