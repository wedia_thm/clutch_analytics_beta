<%@page pageEncoding="ISO-8859-15"  import="java.util.*,wsnoheto.engine.*"%>
<%@taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@taglib prefix="noheto" uri="/WEB-INF/noheto.tld" %>
<noheto:skipPage test="${!pageContext.request.included}"/>
<%-- sys_w2p_template --%>
<div class="dashboardContainer">
    <!-- Tous les graphiques apparaitrons ici -->
</div>

<%
    Set<wsnoheto.log.analytics.EventsLogger.ExtraParam> extraParamNames = new HashSet();
    try {
        for(String objectname:Arrays.asList( "monodocmodel" , "zsnippet" )) {
            extraParamNames.addAll(wsnoheto.log.analytics.EventsLogger.geSysObjectDataEventExtras(objectname));
        }
    } catch (Throwable e) {
        // tant pis
    }
    pageContext.setAttribute("extraParamNames", extraParamNames);
    pageContext.setAttribute("extraSurferParamNames", wsnoheto.log.analytics.EventsLogger.getEventSurferExtras());

%>

<script type="text/javascript">
    /*
    context courant
    */
    var configurationName="sys_objectdata";

    function createSearchOption() {
        var search_option = Analytics.SearchOption()
            .setConfigurationName(configurationName)
            //.putSort( Analytics.FieldSort("@timestamp",false) ) sinon pas de cache
            .setSize(0); // si !=0 alors pas de cache
        search_option.getQuery()
            .mustIn("event.params.objectname", ["monodocmodel","zsnippet"])
        ;
        return search_option;
    }

    $(document).ready(function () {
        var dashboardname = "sys_w2p_template";

        BOV3AnalyticsHelper.addHistogram({
            fieldname:"@timestamp",
            label:boi18n.getMessage( "/bov3/wcm", "analytics.dashboard."+ dashboardname + ".chart.histogram" + ".aggregationlabel" ),
            title:boi18n.getMessage( "/bov3/wcm", "analytics.dashboard."+ dashboardname + ".chart.histogram" + ".title" )
            ,classHeight:"analytics-grid-bloc_height_full"

            ,subAggs:[
                null
                , {name:"event.params.actionid",fieldname:"event.params.actionid",method:Analytics.TermsAggregation,aggregationOptions:{stacked:true,visible:false}}
                , {name:"event.params.objectname",fieldname:"event.params.objectname",method:Analytics.TermsAggregation,aggregationOptions:{stacked:true,visible:false}}
                <%--
                , {name:"event.params.damobject_type",fieldname:"event.params.damobject_type",method:Analytics.TermsAggregation,aggregationOptions:{stacked:true,visible:false}}
                , {name:"event.context.session.type",fieldname:"event.context.session.type",method:Analytics.TermsAggregation,aggregationOptions:{stacked:true,visible:false}}
                , {name:"event.params.statusuid",fieldname:"event.params.statusuid",method:Analytics.TermsAggregation,aggregationOptions:{stacked:true,visible:false}}

                <c:forEach items="${extraSurferParamNames}" var="extraSurferParamName">
                    ,{
                        name:"event.context.surfer.extra.${extraSurferParamName.getAnalyticsShortname()}",
                        fieldname:"event.context.surfer.extra.${extraSurferParamName.getAnalyticsShortname()}",
                        method:Analytics.TermsAggregation,
                        aggregationOptions: {
                            visible:false,
                            stacked:true,
                            label:boi18n.getMessage( "/bov3/wcm", "analytics.dashboard.surfer" )+ " ${noheto:i18nObjectField(wcmFieldnameBundle, extraSurferParamName.getField())}"
                        }
                    }
                </c:forEach>
                <c:forEach items="${extraParamNames}" var="extraParamName">
                    ,{
                        name:"event.params.${extraParamName.getAnalyticsShortname()}",
                        fieldname:"event.params.${extraParamName.getAnalyticsShortname()}",
                        method:Analytics.TermsAggregation,
                        aggregationOptions: {
                            visible:false,
                            stacked:true,
                            label:"${noheto:i18nObjectField(wcmFieldnameBundle, extraParamName.getField())}"
                        }
                    }
                </c:forEach>
                --%>
            ]

        });

        BOV3AnalyticsHelper.addTermsAgg({fieldname:"event.params.objectname"});
        BOV3AnalyticsHelper.addTermsAgg({fieldname:"event.params.actionid"});
        BOV3AnalyticsHelper.addTermsAgg({fieldname:"event.params.objectuid", mode:"table"});


        <%--
        Ajoutons tous les propri�t�s marqu�es avec analytics/sysobjectdata/extra
        --%>
        <c:forEach items="${extraParamNames}" var="extraParamName">
        BOV3AnalyticsHelper.addTermsAgg({
            fieldname:"event.params.${extraParamName.getAnalyticsShortname()}", mode:"table", max:50,
            title:"${noheto:i18nObjectField(wcmFieldnameBundle, extraParamName.getField())}"
        });
        </c:forEach>
        <%--
        <c:forEach items="${extraSurferParamNames}" var="extraSurferParamName">
            BOV3AnalyticsHelper.addTermsAgg({
                fieldname:"event.context.surfer.extra.${extraSurferParamName.getAnalyticsShortname()}", mode:"table", max:50,
                title:boi18n.getMessage( "/bov3/wcm", "analytics.dashboard.surfer" )+ " ${noheto:i18nObjectField(wcmFieldnameBundle, extraSurferParamName.getField())}"
            });
        </c:forEach>
        --%>

        Analytics.Manager().refresh();
    });
</script>
