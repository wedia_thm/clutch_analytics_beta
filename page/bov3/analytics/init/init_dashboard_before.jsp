<%@page pageEncoding="ISO-8859-15" import="wsnoheto.log.analytics.*,wsnoheto.log.analytics.search.*,java.util.*"%>
<%@ page import="wsnoheto.tools.StringRef" %>
<%@ page import="wsnoheto.config.CTConfig" %>
<%@taglib prefix="noheto" uri="/WEB-INF/noheto.tld" %>
<noheto:skipPage test="${!pageContext.request.included}"/>
<%
System.out.println("aaa");
    if(CTConfig.C_PARAM_DEV_LOG_ACTION_ON_WORKFLOW_PROCESS) return;
    // on est pas sur une 11.17+

    com.noheto.plugins.IPlugin plugin = com.noheto.plugins.PluginHelper.fromRequest(request);

    List<String> dbs = new ArrayList<String>();
    if (plugin.getParameter("showAnalyticsDashboardTemplate").getValueWithDefaultAsBoolean()){
        dbs.add("clutch_w2p_template");
    }
    if (plugin.getParameter("showAnalyticsDashboardDocument").getValueWithDefaultAsBoolean()){
        dbs.add("clutch_w2p_document");
    }
    if (dbs.isEmpty()) return;
    
    StringRef robotsan = CTConfig.getParameter(CTConfig.C_PARAM_COMPOSITION_SAN);
    if (robotsan == null || robotsan.toString().length() == 0) return;

    // on verifie tout d'abord que ES est disponible
    EventsService eventsService = EventsService.getInstance();
    boolean isElasticAvailable = eventsService.isConfigured();
    if ( !isElasticAvailable ) return;


    java.util.List<com.noheto.bov3.AnalyticDashboard> dashboards = (java.util.List<com.noheto.bov3.AnalyticDashboard>)request.getAttribute( "analytics_dashboards" );
    if (dashboards == null) dashboards = new java.util.ArrayList<com.noheto.bov3.AnalyticDashboard>();


    //Arrays.asList( "clutch_w2p_template" , "clutch_w2p_document" );
    for( com.noheto.bov3.AnalyticDashboard dashboard : dashboards ){
        if (dashboard.getId().equalsIgnoreCase( "sys_w2p_template" )){
            dbs.remove("clutch_w2p_template");
        }else if(dashboard.getId().equalsIgnoreCase( "clutch_w2p_document" )){
            dbs.remove("clutch_w2p_document");
        }
    }
    for(String id : dbs){
        com.noheto.bov3.AnalyticDashboard dashboard = new com.noheto.bov3.AnalyticDashboard(id)
            .setUrl( String.format("%s/bov3/analytics/content/dashboards/%s.jspz" , plugin.getPagePath() , id) )
            .setVisible(true);
        dashboards.add(dashboard);
    }

    request.setAttribute("analytics_dashboards" , dashboards);


%>